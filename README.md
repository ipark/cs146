CS146. Data Structures and Algorithms (Fall2018 @ SJSU)
========

Textbook: Cormen, Leiserson, Rivest and Stein, Introduction to Algorithms, 3rd Edition
-----

References:
1. Data structures and abstractions with Java / Frank M. Carrano. -- 3rd ed.

2. Data structures : abstraction and design using Java / Elliot B. Koffman, Paul A.T. Wolfgang -- 3rd ed.

3. Data structures and algorithm analysis in Java / Mark Allen Weiss. – 3rd ed.

Topics
-------
1. Review Data Structures (lists, stacks, queues,trees), recursion, basic algorithms

2. Insertion Sort, Analyzing and Designing Algorithms 

3. Divide and Conquer technique: Merge Sort

4. Algorithm Analysis and Asymptotic Notation

5. Solving Recurrences - Master Theorem

6. Intro to Heaps and Priority Queue

7. Heapsort

8. Quicksort and Radix sort

9. Analysis of Quicksort

10. Hashing (linear, quadratic and double hashing), separate chaining

11. Binary Search Tree 

12. AVL Trees

13. Red Black trees 

14. Dynamic Programming technique

15. Greedy technique 

16. Graph Algorithms, BFS, DFS 

17. Minimum Spanning Tree; The Disjoint Set Class (Kruskal's algorithm)

18. Single Source Shortest Paths: Dijkstra’s Algorithm

19. All-Pairs Shortest Paths: Floyd-Warshall

