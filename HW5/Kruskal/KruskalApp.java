import java.util.*;

public class KruskalApp {

  public static void main(String[] args) {

    ////////////////////////////////////////////////
    // Example Graph in the Lecture Note
    ////////////////////////////////////////////////
    /*
    String[] vertexList = {"v1", "v2", "v3", "v4", "v5", "v6", "v7"};
    int numVertices = vertexList.length;

    List<Edge> edges = new ArrayList<>();
    edges.add(new Edge(0, 3, 1)); // v1v4=1
    edges.add(new Edge(5, 6, 1)); // v6v7=1
    edges.add(new Edge(0, 1, 2)); // v1v2=2
    edges.add(new Edge(2, 3, 2)); // v3v4=2
    edges.add(new Edge(1, 3, 3)); // v2v4=3
    edges.add(new Edge(0, 2, 4)); // v1v3=4
    edges.add(new Edge(3, 6, 4)); // v4v7=4
    edges.add(new Edge(2, 5, 5)); // v3v6=5
    edges.add(new Edge(4, 6, 6)); // v5v7=6
    */
    ////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////
    /*
    VertexIndex:    0   1   2   3   4   5   6
    Vertex Name:   v1  v2  v3  v4  v5  v6  v7
    Minimum Spanning Tree
    (0, 3) = 1
    (5, 6) = 1
    (2, 3) = 2
    (0, 1) = 2
    (3, 6) = 4
    (4, 6) = 6
    */

    ////////////////////////////////////////////////
    // Given Grap for HW5
    ////////////////////////////////////////////////
    String[] vertexList = {"A", "B", "C", "D", "E"};
    int numVertices = vertexList.length;

    List<Edge> edges = new ArrayList<>();
    edges.add(new Edge(0, 1, 50)); // AB 50
    edges.add(new Edge(0, 3, 80)); // AD 80
    edges.add(new Edge(1, 2, 60)); // BC 60
    edges.add(new Edge(1, 3, 90)); // BD 90
    edges.add(new Edge(2, 4, 40)); // CE 40
    edges.add(new Edge(3, 2, 20)); // DC 20
    edges.add(new Edge(3, 4, 70)); // DE 70
    edges.add(new Edge(4, 1, 50)); // EB 50
    ////////////////////////////////////////////////
    // OUTPUT
    ////////////////////////////////////////////////
    /*
    VertexIndex:    0   1   2   3   4
    Vertex Name:    A   B   C   D   E
    Minimum Spanning Tree
    (3, 2) = 20
    (2, 4) = 40
    (4, 1) = 50
    (0, 1) = 50
    */

    System.out.print("VertexIndex: " );
    for (int i = 0; i < numVertices; i++)
      System.out.printf("%4d", i);
    System.out.println();

    int i = 0;
    System.out.print("Vertex Name: " );
    for (String s : vertexList) 
      System.out.printf("%4s", s);
    System.out.println();

    System.out.println("Minimum Spanning Tree");
    List<Edge> minSpanTree =  Kruskal.mst(edges, numVertices);
    for (Edge e : minSpanTree) 
      System.out.println(e);
  } // EO-main

}
