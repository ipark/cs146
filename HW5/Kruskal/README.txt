2- Assume the above graph is undirected path. Find the minimum spanning tree using disjoint set technique. Write the program and also show this using the paper pencil technique (using the steps shown in lecture notes).

    /** Adjacency Matrix
     *    A0 B1 C2 D3 E4
     * A0 .  50 .  80 . 
     * B1 .  .  60 90 .
     * C2 .  .  .  .  40
     * D3 .  .  20 .  70
     * E4 .  50 .  .  .
     */
