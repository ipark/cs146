import java.util.*;

public class Edge implements Comparable<Edge> {

  /** 
   * vertex v and w
   */
  private int v;
  private int w;
  private int weight;

  public Edge(int v, int w, int weight) {
    this.v = v;           
    this.w = w;
    this.weight = weight;
  }

  public int getv() {
    return v;
  }

  public int getw() {
    return w;
  }

  public int getWeight() {
    return weight;
  }

  @Override
  public int compareTo(Edge otherEdge) {
    return this.weight == otherEdge.weight ? 0 : (
           this.weight < otherEdge.weight ? -1 : 1);
    //return 0;
  }
}
