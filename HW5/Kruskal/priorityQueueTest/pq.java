import java.util.*;

public class pq {
  public static void main(String[] args) {

    List<Edge> edges = new ArrayList<>();
    edges.add(new Edge(0, 1, 10));
    edges.add(new Edge(1, 2, 5));
    edges.add(new Edge(2, 3, 25));
    edges.add(new Edge(0, 2, 2));
    edges.add(new Edge(3, 2, 80));
    //PriorityQueue<Edge> pq = new PriorityQueue<>(10, (e1, e2) -> e1.getWeight() - e2.getWeight());
    PriorityQueue<Edge> pq = new PriorityQueue<>(edges);
    /*
    pq.add(new Edge(0, 1, 10));
    pq.add(new Edge(1, 2, 5));
    pq.add(new Edge(2, 3, 25));
    pq.add(new Edge(0, 2, 2));
    pq.add(new Edge(3, 2, 80));
    */
    while (pq.size() != 0)
      System.out.println(pq.remove().getWeight());
  }
}
