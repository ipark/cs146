public class Graph {

  private final int MAX_VERTS = 20;
  private final int INFINITY = 1000000;
  private Vertex vertexList[]; // list of vertices
  private int adjMatrix[][]; // adjacency matrix
  private int nVerts; // current number of vertices
  private int nTree; // number of vertices inn tree
  private DistanceParent shortestPath[]; // array for shortest-path data
  private int currentVertex; // current vertex
  private int startToCurrent; // distance to currentVertex

  public Graph() {
    vertexList = new Vertex[MAX_VERTS];
    adjMatrix = new int[MAX_VERTS][MAX_VERTS];
    nVerts = 0;
    nTree = 0;
    for (int j = 0; j < MAX_VERTS; j++)
     for (int k = 0; k < MAX_VERTS; k++)
      adjMatrix[j][k] = INFINITY;
    shortestPath = new DistanceParent[MAX_VERTS]; 
    //DistanceParent.distance
    //DistanceParent.parentVertex
  }

  public void addVertex(char label) {
    vertexList[nVerts++] = new Vertex(label);
    //Vertex.label
    //Vertex.isInTree = false;
  }

  public void addEdge(int start, int end, int weight) {
    adjMatrix[start][end] = weight; // directed
  }

  /**
   * find all shorted paths
   * HW5-Q1: 
   * 1) Write the Dijkstra's code to find the minimum distance from
   *    each node to all other nodes.
   * 2) The output of the doe must specify each starting node 
   *    and the path from source node to each of the destinations.
   * 3) Note that the program must have a while loop to change the
   *    source at each new iteration of the while loop
   */
  public void path() {
    int startTree = 0;

    /** 
     * HW5-Q1: 
     * 3) Note that the program must have a while loop to change 
     *    the source at each new iteration of the while loop
     */
    boolean nextVert = true; // boolean flag to continue loop or not
    while (true) { // true until startTree == nVerts
      ///////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////
      vertexList[startTree].isInTree = true;
      nTree = 1; // put it in tree
      // transfert row of distances from adjMatrix to shortestPath 
      for (int j = 0; j < nVerts; j++) {
        int tempDist = adjMatrix[startTree][j];
        shortestPath[j] = new DistanceParent(startTree, tempDist);
      }
      /** 
       * HW5-Q1: 
       * set the source node distance 0
       */
      shortestPath[startTree].distance = 0;
                                                                    
      // until all vertices are in the tree
      while (nTree < nVerts) {
        int indexMin = getMin(); // get minimum from startTree
        int minDist = shortestPath[indexMin].distance;
        
        if (minDist == INFINITY) {
          System.out.println("there are unreachable vertices");
          break; // shortestPath is complete
        }
        else {
          // reset currentVertex
          currentVertex = indexMin; // to closest vertex
          startToCurrent = shortestPath[indexMin].distance;
          // minimum distance from startTree is to currentVertex,
          // and is startToCurrent
        }
        // put current vertex in tree
        vertexList[currentVertex].isInTree = true;
        nTree++;
        adjust_shortestPath(); // update shortestPath[] array
      } // EO-while(nTree < nVerts)
      ///////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////
      System.out.println("\nDijkstra Final Table....");
      System.out.print("SourceVert["+ vertexList[startTree].label + "] "); 
      System.out.println("via (Path Nodes) to DestVert");
      displaySrcPathsDes(startTree);

      nTree = 0;
      for (int i = 0; i < nVerts; i++)
        vertexList[i].isInTree = false;

      startTree++;

      if (startTree == nVerts) {
        nextVert = false;
        break;
      }
      //////////////////////////////////////////////
    } // EO-while(true)
  } // EO-path()


  public int getMin() {
    int minDist = INFINITY;
    int indexMin = 0;
    for (int j = 1; j < nVerts; j++) {
      if (!vertexList[j].isInTree && shortestPath[j].distance < minDist) {
        minDist = shortestPath[j].distance;
        indexMin = j;
      }
    } // EO-for
    return indexMin;
  } // EO-getMin()
    
  public void adjust_shortestPath() {
    int column = 1; // skip starting vertex
    while (column < nVerts) {
      if (vertexList[column].isInTree) {
        column++;
        continue;
      }
      int currentToFringe = adjMatrix[currentVertex][column];
      int startToFringe = startToCurrent + currentToFringe;
      int shortestPathDistance = shortestPath[column].distance;

      if (startToFringe < shortestPathDistance) {
        shortestPath[column].parentVertex = currentVertex;
        shortestPath[column].distance = startToFringe;
      }
      column++;
    } // EO-while
  } // EO-adjust_shortestPath()

  public void displayPaths() {
    for (int j = 0; j < nVerts; j++) {
      System.out.print(vertexList[j].label + "="); // B=
      if (shortestPath[j].distance == INFINITY)
        System.out.print("inf");
      else
        System.out.print(shortestPath[j].distance);
      char parent = vertexList[shortestPath[j].parentVertex].label;
      System.out.print("(" + parent + "), ");
      System.out.println(vertexList[j].isInTree);

    }
  } // EO-displayPaths

  /**
   *
   * HW5-Q1: 
   * 2) The output of the doe must specify each starting node 
   *    and the path from source node to each of the destinations.
   *
   */
  public void displaySrcPathsDes(int srcVertex) {
    for (int j = 0; j < nVerts; j++) {

      /**
       * source vertex label
       */
      char srcVertexLabel = vertexList[srcVertex].label; // source vertex
      System.out.print("[" + srcVertexLabel + "]");

      /**
       * parent vertex label
       * i.e. path nodes from source to destination
       */
      char parentLabel = vertexList[shortestPath[j].parentVertex].label;
      if (parentLabel != srcVertexLabel) 
        System.out.print("---(" + parentLabel + ")--->");
      else 
        System.out.print("--------->");

      /**
       * destination vertex label
       */
      char targetVertexLabel = vertexList[j].label; // destination vertex
      System.out.print(targetVertexLabel);

      if (shortestPath[j].distance == INFINITY)
        System.out.println(" = inf");
      else
        System.out.println(" = " + shortestPath[j].distance);
    }
  } // EO-displayPaths

} // EO-Graph
