1- Given the following graph, write the Dijkstra’s code to find the minimum distance from each node to all other nodes. The output of the code must specify each starting node and the path from source to each of the destinations. In addition, using paper pencil technique (the steps used in the lecture) find the shortest path having node-C as the source. Note that the program must have a while loop to change the source at each new iteration of the while loop.
    /** Adjacency Matrix
     *    A0 B1 C2 D3 E4
     * A0 .  50 .  80 . 
     * B1 .  .  60 90 .
     * C2 .  .  .  .  40
     * D3 .  .  20 .  70
     * E4 .  50 .  .  .
     */
