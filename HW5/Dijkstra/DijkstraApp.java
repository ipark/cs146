public class DijkstraApp {

  public static void main(String[] args) {

    Graph theGraph = new Graph();

    /**
     *    A0 B1 C2 D3 E4
     * A0 .  50 .  80 . 
     * B1 .  .  60 90 .
     * C2 .  .  .  .  40
     * D3 .  .  20 .  70
     * E4 .  50 .  .  .
     */
    theGraph.addVertex('A'); // 0 - start
    theGraph.addVertex('B'); // 1
    theGraph.addVertex('C'); // 2
    theGraph.addVertex('D'); // 3
    theGraph.addVertex('E'); // 4

    theGraph.addEdge(0, 1, 50); // AB 50
    theGraph.addEdge(0, 3, 80); // AD 80
    theGraph.addEdge(1, 2, 60); // BC 60
    theGraph.addEdge(1, 3, 90); // BD 90
    theGraph.addEdge(2, 4, 40); // CE 40
    theGraph.addEdge(3, 2, 20); // DC 20
    theGraph.addEdge(3, 4, 70); // DE 70
    theGraph.addEdge(4, 1, 50); // EB 50

    System.out.println("Shorted Paths");

    /**
      * HW5-Q1: 
      * 1) Write the Dijkstra's code to find the minimum distance from 
      *    each node to all other nodes.
      * 2) The output of the doe must specify each starting node 
      *    and the path from source node to each of the destinations.
      * 3) Note that the program must have a while loop to change the
      *    source at each new iteration of the while loop
      */
    theGraph.path(); // Dijkstra's code 

    System.out.println();
  }
}
