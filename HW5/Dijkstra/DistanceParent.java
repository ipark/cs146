public class DistanceParent {
  
  public int distance;     // distance from start to this vertex
  public int parentVertex; // current parent of this vertex

  public DistanceParent(int pv, int d) {
    distance = d;
    parentVertex = pv;
  }
}

