========= TEST1 =========
Ininial Table Size = 7; Data Size = 6
hash(19) = 5
hash(34) = 6
hash(49) = 0
hash(16) = 2
hash(32) = 4
hash(51) = 2
HashTable for Separate Chaining (Size = 7)
0: [49]
1: []
2: [16, 51]
3: []
4: [32]
5: [19]
6: [34]


========= TEST2 =========
Ininial Table Size = 7; Data Size = 12
hash(19) = 5
hash(34) = 6
hash(49) = 0
hash(16) = 2
hash(32) = 4
hash(51) = 2
hash(119) = 0
hash(134) = 15
hash(149) = 13
hash(116) = 14
hash(132) = 13
hash(151) = 15
HashTable for Separate Chaining (Size = 17)
7: [119, 51, 34]
8: []
9: [19]
10: []
11: []
12: []
13: []
14: []
15: []
16: []
17: []
18: []
19: []
20: [149, 132]
21: [116]
22: [49, 32, 134, 151]
23: [16]
