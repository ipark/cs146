import java.util.*;

public class SeparateChainingHashTableTester {
  public static void main(String [] args) {
    int hashTableSize = 7;
    /////////////////////
    SeparateChainingHashTable<Integer> H = 
      new SeparateChainingHashTable<>(hashTableSize);

    List<Integer> data1 = Arrays.asList(19, 34, 49, 16, 32, 51);
    System.out.println("========= TEST1 =========");
    System.out.println("Ininial Table Size = 7; Data Size = " + data1.size()); 
    for (Integer i : data1) {
      System.out.println("hash(" + i + ") = " + H.checkMyHashValue(i)); 
      H.insert(i);
    }
    System.out.println("HashTable for Separate Chaining (Size = " + 
        H.hashTableSize() + ")");
    H.printHashTable();

    List<Integer> data2 = Arrays.asList(19, 34, 49, 16, 32, 51, 119, 134, 149, 116, 132, 151);
    System.out.println("\n\n========= TEST2 =========");
    System.out.println("Ininial Table Size = 7; Data Size = " + data2.size()); 
    for (Integer i : data2) {
      System.out.println("hash(" + i + ") = " + H.checkMyHashValue(i)); 
      H.insert(i);
    }
    System.out.println("HashTable for Separate Chaining (Size = " + 
        H.hashTableSize() + ")");
    H.printHashTable();
  }
}

/** OUTPUT
 *
========= TEST1 =========
Ininial Table Size = 7; Data Size = 6
hash(19) = 5
hash(34) = 6
hash(49) = 0
hash(16) = 2
hash(32) = 4
hash(51) = 2
HashTable for Separate Chaining (Size = 7)
0: [49]
1: []
2: [16, 51]
3: []
4: [32]
5: [19]
6: [34]


========= TEST2 =========
Ininial Table Size = 7; Data Size = 12
hash(19) = 5
hash(34) = 6
hash(49) = 0
hash(16) = 2
hash(32) = 4
hash(51) = 2
hash(119) = 0
hash(134) = 1
hash(149) = 13
hash(116) = 14
hash(132) = 13
hash(151) = 15
HashTable for Separate Chaining (Size = 17)
0: [119, 51, 34]
1: []
2: [19]
3: []
4: []
5: []
6: []
7: []
8: []
9: []
10: []
11: []
12: []
13: [149, 132]
14: [116]
15: [49, 134, 32, 151]
16: [16]
 */
