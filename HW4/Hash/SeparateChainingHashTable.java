/**
*  Fall 2018 - CS146 - HW4
*  @author INHEE PARK
*
* 6- The separate chaining code is given in the lecture files 
*    of this week on Canvas. Add an extra method for rehash 
*    and test the rehash method in main.
*
**********************************************************************
* NOTE: 
* The provided code implemented a data structure of Array of LinkedList,
*
*   private List<T>[] theLists; // Array of LinkedList
*
* procuding the following warning: [rawtypes] found raw type: LinkedList
*       theLists = new LinkedList[nextPrime(size)];
*                      ^
*
* Therefore, I refactored the given code by implementing the data structure
* of ArrayList of LinkedList:
*
*   private List<List<T>> theLists; // better to be ArrayList of LinkedList
*
*/

import java.util.*;

public class SeparateChainingHashTable<T> {

  /**
   *  private instance fields
   */
  //private List<T>[] theLists; // Array of LinkedList
  /**
   * Refactored the agove Array of LinedList 
   * to ArrayList of LinkedList 
   */
  private List<List<T>> theLists; // ArrayList of LinkedList
  private int currentSize;
  private static final int DEFAULT_TABLE_SIZE = 101; 

  /** 
   * Constructors
   * 1. No argument is given, then use a default table size
   * 2. Argument of a size is given, create a List of LinkedList
   */
  // constructor 1
  public SeparateChainingHashTable() { // no size is given
    this(DEFAULT_TABLE_SIZE);
  }

  // constructor 1
  public SeparateChainingHashTable(int size) {
    //theLists = new LinkedList[nextPrime(size)]; // Array of LinkedList
    //for (int i = 0; i < theLists.length; i++)
    //  theLists[i] = new LinkedList<>();
    /** 
     * Refactored the above Array of LinedList
     * to ArrayList of LinkedList as following:
     */
    theLists = new ArrayList<List<T>>(); // ArrayList of LinkedList
    for (int i = 0; i < nextPrime(size); i++)
      theLists.add(new LinkedList<T>());
  }

  /** 
   * Find an item in the hash table.
   * @param  x the item to search for
   * @return true if x is found
   */
  public boolean contains(T x) { 
    List<T> whichList = theLists.get(myhash(x));
    return whichList.contains(x); 
  }

  /**
   * Insert into the hash table.
   * If an itme is already in the hash table, DO-NOTHING.
   * @param x the item to insert
   */
  public void insert(T x) {
    List<T> whichList = theLists.get(myhash(x));
    /**
     * IF x is new item, 
     *  1. insert into hash table
     *  2. if LOAD FACTOR > 0.5, ((REHASH))
     * ELSE // x is already in the hash table
     *  do nothing
     */
    if (!whichList.contains(x)) { // when x is new item
      whichList.add(x);
      /**********************************************
       * ((REHASH))
       * load factor = (# of currentSize)/(TableSize) 
       * if load factor > 0.5, call rehash() method
       **********************************************/
      currentSize++;
      if (loadFactor() > 0.5)
        rehash();
    } // EO-if
  }

  public void remove(T x) { 
    List<T> whichList = theLists.get(myhash(x));
    if (whichList.contains(x)) {
      whichList.remove(x);
      currentSize--;
    }
  }

  private int myhash(T x) { 
    /**
     * x.hashCode(), 
     * where the hashCode() method is from Java standard library 
     * java.lang
     *  public class Object
     *    public int hashCode()
     *    Returns a hash code value for the object. 
     * Therefore, I don't need to use the provided 
     *    "hash(String key, int TableSize)" method.
     */
    int hashVal = x.hashCode(); // hasCode() is from Java standard library
    int tableSize = theLists.size(); 
    hashVal %= tableSize;
    if (hashVal < 0)
      hashVal += tableSize;
    return hashVal;
  }

  private static int nextPrime(int n) {
    if (n % 2 == 0)
      n++;
    for ( ; !isPrime(n); n += 2)
      ;
    return n;
  }
  public static boolean isPrime(int n) {
    if (n == 2 || n == 3)
      return true;
    if (n == 1 || n % 2 == 0)
      return false;
    for (int i = 3; i * i <= n; i +=2)
      if (n % i == 0)
        return false;

    return true;
  }

  /**********************************************
   * REHASH
   * 1. first copy the current hash table to temp data structure:
   * List<List<T>> old_theLists = theLists;
   *
   *
   * 2. find the next prime number, which should be greater than 
   *    the twice of the current hash table size:
   * int newSize = nextPrime(2 * old_theLists.size());
   *
   * 3. create new ArrayList of LinkedList<T> with double newSize 
   *    and initalize it:
   * // allocate new large size
   * theLists = new ArrayList<List<T>>();
   * // initalize
   * for (int i = 0; i < newSize; i++)
   *   theLists.add(new LinkedList<T>()); 
   *
   * 4. copy over the old hash table to new hash table.
   *    hash table data structure is ArrayList of LinkedList
   *    as following:
   *
   *    List<List<T>> old_theLists;                
   *         List<T>  old_whichList;
   *              T   x
   *
   *    for (List<T> old_whichList : old_theLists)
   *      for (T x : old_whichList)
   *        insert(x);
   *
   **********************************************/
  private void rehash() {
    /** 
     * 1. first copy the current hash table to temp
     *    data structure:
     */
    List<List<T>> old_theLists = theLists;
    /** 
     * 2. find the next prime number, which should be
     *    greater than the twice of the current hash 
     *    table size:
     */
    int newSize = nextPrime(2 * old_theLists.size());
    
    /**
     * 3. create new ArrayList of LinkedList<T> with double newSize and initalize it:
     */
    // allocate new large size
    theLists = new ArrayList<List<T>>();
    // initalize
    for (int i = 0; i < newSize; i++)
      theLists.add(new LinkedList<T>()); 

    /** 
      * 4. copy over the old hash table to new hash table,
      *    the data structure of hash table is ArrayList
      *    of LinkedList:
      */
    // reset currentSize, once insert(x) is called currentSize is increased
    currentSize = 0; 
    // List<List<T>> old_theLists;
    //      List<T>  old_whichList;
    //           T   x
    for (List<T> old_whichList : old_theLists) 
      for (T x : old_whichList)
        insert(x); // currentSize is increased by insert method
  }


  /**********************************************
   * load factor = (# of currentSize)/(TableSize) 
   * where the TableSize is determined by theLists.size()
   * @return loadFactor 
   **********************************************/
  private double loadFactor() {
    double loadFactor;
    loadFactor = currentSize / theLists.size();
    return loadFactor;
  }

  /**********************************************
   * Check myhash value
   * @param x the item to insert
   * @return myhash value
   **********************************************/
  public int checkMyHashValue(T x) {
    return myhash(x);
  }

  /**********************************************
   * Find the current hash table size regardless of
   * before or after rehashing
   * @return theLists.size()
   **********************************************/
  public int hashTableSize() {
    return theLists.size();
  }

  /**********************************************
   * Print hash table, whose data structure is
   * ArrayList of LinkedList as following:
   * List<List<T>> theLists
   *      List<T>  linkedlist
   **********************************************/
  int i = 0;
  public void printHashTable() {
    for (List<T> linkedlist : theLists)
        System.out.println(i++ + ": " + linkedlist);
  }

}
