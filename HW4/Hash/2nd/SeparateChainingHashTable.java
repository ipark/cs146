import java.util.*;

public class SeparateChainingHashTable<T> {

    private int DEFAULT_TABLE_SIZE;

    /** The array of Lists. */
    //private List<T> [] theLists; 
    private List<List<T>> theLists; 
    private int currentSize;

    /**
     * Construct the hash table.
     */
    public SeparateChainingHashTable(int size) {
        DEFAULT_TABLE_SIZE = size;
        //theLists = new LinkedList[nextPrime(size)];
        //for (int i = 0; i < theLists.length; i++)
        //  theLists[i] = new LinkedList<>();
        theLists = new ArrayList<List<T>>();
        for (int i = 0; i < nextPrime(size); i++)
          theLists.add(new LinkedList<T>());
    }


    /**
     * Insert into the hash table. If the item is
     * already present, then do nothing.
     * @param x the item to insert.
     */
    public void insert(T x) {
        //List<T> whichList = theLists[myhash(x)];
        List<T> whichList = theLists.get(myhash(x));
        if(!whichList.contains(x)) {
            whichList.add(x);

                // Rehash; see Section 5.5
            //if(++currentSize > theLists.length)
            if(++currentSize > theLists.size())
                rehash();
        }
    }

    public void printHashTable() {
      int i = 0;
      for (List<T> t : theLists)
          System.out.println(i++ + ": " + t);
    }

    public int finalHashTableSize() {
      //return theLists.length;
      return theLists.size();
    }

    /**
     * Remove from the hash table.
     * @param x the item to remove.
     */
    /*
    public void remove(T x) {
        List<T> whichList = theLists[myhash(x)];
        if(whichList.contains(x)) {
        whichList.remove(x);
            currentSize--;
        }
    }
    */

    /**
     * Find an item in the hash table.
     * @param x the item to search for.
     * @return true if x isnot found.
     */
    public boolean contains(T x) {
        //List<T> whichList = theLists[myhash(x)];
        List<T> whichList = theLists.get(myhash(x));
        return whichList.contains(x);
    }

    /**
     * Make the hash table logically empty.
     */
    /*
    public void makeEmpty() {
        for (int i = 0; i < theLists.length; i++)
            theLists[i].clear();
        currentSize = 0;    
    }
    */

    /**
     * A hash routine for String objects.
     * @param key the String to hash.
     * @param tableSize the size of the hash table.
     * @return the hash value.
     */
    /*
    public static int hash(String key, int tableSize) {
        int hashVal = 0;

        for (int i = 0; i < key.length(); i++)
            hashVal = 37 * hashVal + key.charAt(i);

        hashVal %= tableSize;
        if(hashVal < 0)
            hashVal += tableSize;

        return hashVal;
    }
    */

    private void rehash() {
        //List<T> []  oldLists = theLists;
        List<List<T>> oldLists = theLists;

            // Create new double-sized, empty table
        //theLists = new List[nextPrime(2 * theLists.length)];
        theLists = new ArrayList<List<T>>();
        for (int j = 0; j < nextPrime(2 * oldLists.size()); j++)
          //theLists[j] = new LinkedList<>();
          theLists.add(new LinkedList<T>());

            // Copy table over
        currentSize = 0;
        for (List<T> list : oldLists)
            for (T item : list)
                insert(item);
    }

    // public helper method for myhash()
    public int CheckHashValue(T x) {
      return myhash(x);
    }
    private int myhash(T x) {
        int hashVal = x.hashCode();

        //hashVal %= theLists.length;
        hashVal %= theLists.size();
        if(hashVal < 0)
            //hashVal += theLists.length;
            hashVal += theLists.size();

        return hashVal;
    }
    

    /**
     * Internal method to find a prime number at least as large as n.
     * @param n the starting number (must be positive).
     * @return a prime number larger than or equal to n.
     */
    private static int nextPrime(int n) {
        if(n % 2 == 0)
            n++;

        for (; !isPrime(n); n += 2)
            ;

        return n;
    }

    /**
     * Internal method to test if a number is prime.
     * Not an efficient algorithm.
     * @param n the number to test.
     * @return the result of the test.
     */
    private static boolean isPrime(int n) {
        if(n == 2 || n == 3)
            return true;

        if(n == 1 || n % 2 == 0)
            return false;

        for (int i = 3; i * i <= n; i += 2)
            if(n % i == 0)
                return false;

        return true;
    }

}
