insert 6 into BST
insert 2 into BST
insert 8 into BST
insert 1 into BST
insert 4 into BST
insert 3 into BST
insert 15 into BST
insert 20 into BST

HW Assignment
	Height of BST = 4
	Number of nodes in BST = 8
	In-Order =  1 2 3 4 6 8 15 20
	Post-Order =   1   3  4 2     20 15 8 6

Extra Exercises
	Is this tree balanced? => false
	Average value of all nodes => 7
	Count(even number nodes) => 5
