/**
   Fall 2018 - CS146 - HW4
   @author INHEE PARK
1- Write a program to implement a generic binary search tree.
2- Implement the function printInOrder.
3- Implement a method that returns a String that contains 
   the PostOrder traversal of the binary search tree.
4- Implement a public method that return the height of the binary tree.
5- Implement a public method that return the number of nodes in the binary tree.
6- The separate chaining code is given in the lecture files on Canvas. 
   Add an extra method for rehash and test the rehash method in main.
 */
public class BSTtester {
  public static void main(String[] args) {
    /**
     * NOTE: 
     * even though generic <T> is defined in the class definition
     * you must provide specify wrapper types for your data insertion
     */
    BST<Integer> bst = new BST<>();
       /********/
    Integer[] vals = {6, 2, 8, 1, 4, 3, 15, 20};
    for (Integer i : vals)
      bst.insert(i);
    System.out.println("\nHW Assignment");
    System.out.println("\tHeight of BST = " +  bst.heightBST());
    System.out.println("\tNumber of nodes in BST = " + bst.countNodesBST());                
    System.out.print("\tIn-Order = "); 
    bst.printInOrder(); 
    System.out.println();        
    System.out.print("\tPost-Order = "); 
    System.out.println(bst.toString());            
    System.out.println("\nExtra Exercises");
    System.out.println("\tIs this tree balanced? => " + bst.isBalancedTree());
    System.out.println("\tAverage value of all nodes => " + bst.aveValNodes());
    System.out.println("\tCount(even number nodes) => " + bst.countEvenNodes());
  }
}
        
