/**
   Fall 2018 - CS146 - HW4
   @author INHEE PARK
* 1- Write a program to implement a generic binary search tree.
* 2- Implement the function printInOrder.
* 3- Implement a method that returns a String that contains 
*    the PostOrder traversal of the binary search tree.
* 4- Implement a public method that return the height of the binary tree.
* 5- Implement a public method that return the number of nodes in the binary tree.
*
* ####################################################
* EXTRA methods
    Is this tree balanced?      bst.isBalancedTree()
    Average value of all nodes  bst.aveValNodes()
    Count(even number nodes)    bst.countEvenNodes()
*/

/* 
 * 1- Write a program to implement a generic binary search tree.
 */
public class BST<T extends Comparable<? super T>> { // BST = BinarySearchTree

  /******************************** 
   * Inner class, BinaryNode<T>
   ********************************/
  private static class BinaryNode<T> {
    /** private instance fields */
    private T element;
    private BinaryNode<T> left;
    private BinaryNode<T> right;
    /** constructor-1 */
    BinaryNode(T aElement) {
      this(aElement, null, null);
    }
    /** constructor-2 */
    BinaryNode(T aElement, BinaryNode<T> lt, BinaryNode<T> rt) {
      element = aElement;
      left = lt;
      right = rt;
    }
  } // EO-BinaryNode-class

  /** private instance field for BST */
  private BinaryNode<T> root;

  /** constructor for BST */
  public BST() {
    root = null;
  }

  /******************************** 
   * public helper method for insert
   * without internal private fields as arguments
   */
  public void insert(T x) {
    ///////////////////////
    ///  CRUCIAL!  ////////
    root = insert(x, root);
    ///////////////////////
    System.out.println("insert " + x + " into BST");
  }
  
  /******************************** 
   * private internal method for insert
   * with private fields as arguments 
   * by recursive calls
   */
  private BinaryNode<T> insert(T x, BinaryNode<T> t) {

    if (t == null)
      return new BinaryNode<>(x, null, null); 

    if (x.compareTo(t.element) < 0) // new element x goes to left node
      t.left = insert(x, t.left);
    else if (x.compareTo(t.element) > 0) // new element x goes to right node
      t.right = insert(x, t.right);
    else // duplicate element x
      ;  // do nothing
    return t;
  }
  
  /******************************** 
   * 2- Implement the function printInOrder.
   */
  /** public helper method for printInOrder */
  public void printInOrder() {
    if (root != null)
      printInOrder(root);
  }

  /**
   * private internal method for printInOrder 
   * by recursive calls
   * In-order: left sub-tree -> root -> right sub-tree
   */
  private void printInOrder(BinaryNode<T> t) {
    if (t != null) { ////// CRUCIAL!
      printInOrder(t.left);                
      System.out.print(" " + t.element);
      printInOrder(t.right);
    }
  }

  /******************************** 
   * 3- Implement a method that returns a String that contains 
   */
  /** public helper method for toString of PostOrder */
  @Override
  public String toString() {
    if (root == null)
      return "";
    else
      return toString(root);
  }

  /**
   * private internal method for toString of PostOrder
   * by recursive calls
   * Post-order: left sub-tree -> right sub-tree -> root
   */
  private String toString(BinaryNode<T> t) {
    if (t == null)  ////// CRUCIAL!
      return "";
    else
      return toString(t.left) + " " + toString(t.right) + " " + t.element;
  }

  /******************************** 
   * 4- Implement a public method that return the height of the binary tree.
   */
  /** public helper method for toString of heightBST */
  public int heightBST() {
    if (root == null)
      return 0;
    else
      return 1 + Math.max(heightBST(root.left), heightBST(root.left));
  }

  /** private internal method for heightBST by recursive calls */
  private int heightBST(BinaryNode<T> t) {
    if (t == null) ////// CRUCIAL!
      return 0;
    else
      return (1 + Math.max(heightBST(t.left), heightBST(t.right)));
  }

  /******************************** 
   * 5- Implement a public method that return the number of nodes in the binary tree.
   */
  /** public helper method for toString of countNodesBST() */
  public int countNodesBST() {
    if (root == null) ////// CRUCIAL!
      return 0;
    else
      return 1 + countNodesBST(root.left) + countNodesBST(root.right);
  }

  /** private internal method for countNodesBST by recursive calls */
  private int countNodesBST(BinaryNode<T> t) {
    if (t == null) ////// CRUCIAL!
      return 0;
    else
      return (1 + countNodesBST(t.left) + countNodesBST(t.right));
  }

  
  /**
   *  Is this tree balanced?      bst.isBalancedTree()
   */
  public boolean isBalancedTree()
  {
    if (root != null)
      return isBalancedTreeHelper(root);
    else
      return true;
  }
  private boolean isBalancedTreeHelper(BinaryNode<T> t)
  {
    if (t != null)
    {
      return pass(t) && 
              isBalancedTreeHelper(t.left) &&
              isBalancedTreeHelper(t.right);
    }
    return true;
  }
  private boolean pass(BinaryNode<T> t)
  {
    if (t != null)
    {
      int left, right;
      left = heightBST(t.left);
      right = heightBST(t.right);
      return (Math.abs(left - right) > 1) ? false : true;
    }
    return true;
  }

  /**
   * Average value of all nodes  bst.aveValNodes()
   */
  public int aveValNodes()
  {
    if (root != null)
    {
      int counter = 0;
      int sum = aveValNodesHelper(root, counter++);
      int num = countNodesBST(root);
      return sum/num;
    }
    else
      return 0;
  }
  private int aveValNodesHelper(BinaryNode<T> t, int c)
  {
    if (t != null)
    {
      c++;
      int thisElement = (Integer) t.element;
      int sum = thisElement 
                + aveValNodesHelper(t.left, c)
                + aveValNodesHelper(t.right, c);
      return sum;
    }
    return 0;
  }
  /**
   * Count(even number nodes)    bst.countEvenNodes()
   */
  public int countEvenNodes()
  {
    if (root != null)
    {
      return countEvenNodesHelper(root);
    }
    else
      return 0;
  }
  private int countEvenNodesHelper(BinaryNode<T> t)
  {
    if (t != null)
    {
      int thisElement = (Integer) t.element;
      int even = (thisElement % 2 == 0) ? 1 : 0;
      return (even + countEvenNodesHelper(t.left) + countEvenNodesHelper(t.right));
    }
    return 0;
  }

} // EO-BST-class
