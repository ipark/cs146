/**
 * @author Inhee Park
 * Testing convertToPostfix method of the InfixToPostfix class
 * by benchmarking with 5 infix strings 
 * against their known resulting postfix strings
 * and sanity check whether the convertToPostfix method predicts
 * correctly by comparing its prediction and known result
 */
public class InfixToPostfixTester 
{
    public static void main(String[] args) 
    {
        String[] infixArray = {"a + b * c", "a-b+c", "A+B*(C-D/( E + F))", 
                                "a/b*(c+(d-e))", "A*B^C+D"};
        String[] postfixArray = {"abc*+", "ab-c+", "ABCDEF+/-*+", 
                                "ab/cde-+*", "ABC^*D+"};
        System.out.printf("%-20s%-10s%-20s%-10s\n", 
                            "[ Infix ] ", "---->", "[ Postfix ] ", "[ Check ]");
        int i = 0; // array index
        for (String infix : infixArray)
        {
            String postfix = InfixToPostfix.convertToPostfix(infix);
            if (postfix.equals(postfixArray[i])) 
                System.out.printf("%-20s%-10s%-20s%-10s\n", 
                                    infix, "---->", postfix, "Correct");
            else 
                System.out.printf("%-20s%-10s%-20s%-10s\n", 
                                    infix, "---->", postfix, "Wrong");
            i++;
        }
    }
}
