/**
 * @author Inhee Park
 * Fall2018.CS146.HW1.Number2
 *
 * Convert infix expression to equivalent postfix expresssion
 * using the generic type stack (Stack<T>) implemented 
 * in HW1.Number1 by linked list
 */
public class InfixToPostfix 
{
    /**
     * Set convertToPostfix method as "static" to use it 
     * as an independent utility, aka a convertor 
     * from an input infix string to output postfix string.
     */
    public static String convertToPostfix(String infixStr)  
    {
        /**
         * Stack<T> generic type stack implmented by linkedlist
         */
         Stack<Character> operatorStack = new Stack<>();
         String postfix = "";
 
         /**
          * Operators are stored in a stack
          * topOperator is a tracer of the top index of a stack
          */
         char topOperator;
         /** 
          * infix string is converted to chracter aray 
          * and read one character at a time by for loop
          */
         for (char c: infixStr.toCharArray()) 
         {
                    
            switch (c)                                                                          
            {
                case ' ': break; // ignore space
                case '^': // exponent operator has the highest precedenceScore 3
                    operatorStack.push(c);
                    break;
                case '+':  // lowest precedenceScore 1 
                case '-':  // lowest precedenceScore 1
                case '*':  // next precedenceScore 2
                case '/':  // next precedenceScore 2
                    while (!operatorStack.isEmpty() && 
                            precedenceScore(c) <= precedenceScore(operatorStack.peek()))
                    {
                     /** 
                      * compare the precedenceScore between
                      * a) the next operator by peek() call in the operator stack
                      * b) the current operator "c" that's already parsed from infix
                      * if b) operator is higher precedenceScore
                      * then append it to the postfix and pop() 
                      * prior to pushing a) the current operator "c" 
                      */
                        postfix += operatorStack.peek();
                        operatorStack.pop();
                    }
                    operatorStack.push(c);
                    break;
                case '(':
                    operatorStack.push(c);
                    break;
                case ')': // stack is not empty if infix expression is vaild
                    /**
                     * keep calling pop() and append to the postfix
                     * until finiding the pair ")" parenthesis to exit
                     */
                    topOperator = operatorStack.pop();
                    while (topOperator != '(')
                    {
                        postfix += topOperator;
                        topOperator = operatorStack.pop();
                    }
                    break;
                    /**
                     * all the other characters are regarded as a operand
                     * excluding operators, parenthesis and a space.
                     */
                default: // append variables to postfix
                    postfix += c;
                    break; 
            } // EO-switch
         } // EO-for
         /**
          * when reaching the end of the infix string
          * just pop() the operators and append to the postfix
          */
         while (!operatorStack.isEmpty())
         {
             topOperator = operatorStack.pop();
             postfix += topOperator;
         }
         return postfix;
     } // EO-convertToPostfix(String infix)-method
    /**
     * precedenceScore set the priority score
     * @param op operator
     * @return rank priority of rank
     */
    public static int precedenceScore(char op) 
    {
        int rank = 0;
        switch (op)                                                                          
        {
            case '+': 
            case '-':
                rank = 1;
                break;
            case '*': 
            case '/':
                rank = 2;
                break;
            case '^':
                rank = 3;
                break;
        }
        return rank;
    }
} // EO-class InfixToPostfix 
