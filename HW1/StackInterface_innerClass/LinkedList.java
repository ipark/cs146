import java.lang.NullPointerException;
/**
 * @author Inhee Park
 * Fall2018.CS146.HW1.Number1.
 *
 * Implmentation of Generic Singly LinkedList 
 * in which contains inner class Node
 */
public class LinkedList<T>
{
    /** 
     * Instance Variable of LinkedList<T>
     * Reference variable pointing to Node (head)
     */
    private Node<T> head;
    private T tempData; // generic type for storage of a temporary data for return

    /**
     * Constructor LinkedList<T>
     * Starting Node of linked list, aka head Node 
     * with one instance variable
     */
    public LinkedList() 
    {
        head = null;
    }
    /**
     * Instance Method of LinkedList<T>
     * insert new node from head
     * @param id generic type data
     * create a new node then
     * link its next node and
     * set the head node pointing to this node
     */
    public void insertHead(T id)
    {
        Node<T> newNode = new Node<>(id);
        newNode.next = head;
        head = newNode;
    }
    /**
     * Instance Method of LinkedList<T>
     * Delete a node from head
     * @return T
     */
    public T deleteHead() 
    {
        try 
        {
            // store current node to temp    
            Node<T> temp = head;
            head = head.next;
            tempData = temp.iData;
        }
        catch (NullPointerException exception)
        {
            tempData = null;
            System.err.println("Error: empty");
        }
        return tempData;
    }
    /**
     * Instance Method of LinkedList<T>
     * @return True/False whether LinkedList is empty or not
     */
    public boolean isEmpty() 
    {
        return (head == null);
    }
    /**
     * Instance Method of LinkedList<T>
     * @return data of head node 
     */
    public T peek() 
    {
        return head.iData;
    }
    /**
     * Instance Method of LinkedList<T>
     * display all the data of linkedlist
     * from tailNode(old data) toward headNode(new data)
     * because always new data is inserted at the head node
     */
    public void print() 
    {
        Node<T> current = head;
        while (current != null) 
        {
            current.print();
            current = current.next;  
        }
    }
}
