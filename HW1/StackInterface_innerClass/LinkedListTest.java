public class LinkedListTest 
{
    public static void main(String[] args) 
    {
        LinkedList<Integer> lList = new LinkedList<>();
        lList.insertHead(20);
        lList.insertHead(10);
        lList.insertHead(40);
        lList.insertHead(30);
        lList.insertHead(60);
        lList.print();
        lList.deleteHead();
        lList.deleteHead();
        lList.deleteHead();
        lList.print();
    }
}
