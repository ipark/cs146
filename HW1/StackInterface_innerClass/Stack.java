/**
 * @author Inhee Park
 * Fall2018.CS146.HW1.Number1.
 *
 * Implmentation of methods declared in the StackInterface
 * using the generic LinkedList class in which contains 
 * inner class Node
 */
public class Stack<T> implements StackInterface<T>
{
    /**
     * Generic type LinkedList<T> is used to implement a Stack data structure
     */
    private LinkedList<T> stack;

    // Constructor of Stack<T>
    public Stack()
    {
        stack = new LinkedList<>();
    }
    /**
     * Instance Method of Stack<T>
     * pop() method is implemented by 
     * deleteHead() method of the LinkedList class
     * because Last Input First Out at the head Node
     * @return generic type data from top
     */
    public T pop() 
    {
        return stack.deleteHead();
    }
    /**
     * Instance Method of Stack<T>
     * push() method is implemented by 
     * insertHead() method of the LinkedList class
     * because Last-In-First-Out at the head Node
     * @param newEntry generic type data to the top
     */
    public void push(T newEntry) 
    {
        stack.insertHead(newEntry);
    }
    /**
     * Instance Method of Stack<T>
     * @return generic type data from the top without deletion
     */
    public T peek() 
    {
        return stack.peek();
    }
    /**
     * Instance Method of Stack<T>
     * @return True/False whether Stack is empty or not
     */
    public boolean isEmpty() 
    {
        return stack.isEmpty();
    }
    /**
     * Instance Method of Stack<T>
     * display all the data of Stack
     * from bottom(old data) to top(new data)
     */
    public void print() 
    {
        System.out.println("Stack:top");
        stack.print();
        System.out.println("Stack:bottom");
    }
}
