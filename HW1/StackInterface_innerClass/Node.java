/**
 * Inner class Node
 * with two members
 */
public class Node<T>
{
    /**
     * Instance variables of Node<T> class
     * T iData : generic type data
     * Node<T> next: reference variable pointing to next Node
     */
    public T iData;
    public Node<T> next;
    public Node(T id) 
    /**
     * Constructor of Node<T> class
     */
    {
        iData = id;
    }
    /**
     * Instance Method of Node<T> class
     * display a member of a node
     */
    public void print()
    {
        System.out.println(iData);
    }
} // EO-inner-class-Node<T>

