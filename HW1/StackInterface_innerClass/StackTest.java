public class StackTest 
{
    public static void main(String[] args) 
    {
        Stack<Integer> sList = new Stack<>();

        // push
        System.out.println("#First-In push(1)");
        sList.push(1);
        System.out.println("#         push(10)");
        sList.push(10);
        System.out.println("#         push(100)");
        sList.push(100);
        System.out.println("#         push(1000)");
        sList.push(1000);
        System.out.println("#Last-In  push(10000)");
        sList.push(10000);
        // print stack
        sList.print();
        System.out.println("\n");

        // pop
        System.out.print("# pop(): ");
        System.out.println(sList.pop());
        System.out.print("# pop(): ");
        System.out.println(sList.pop());
        // print stack
        sList.print();
        System.out.println("\n");


        // peek
        System.out.print("peek(): ");
        System.out.println(sList.peek());
        // print stack
        sList.print();
        System.out.println("\n");

        // pop
        System.out.print("# pop(): ");
        System.out.println(sList.pop());
        System.out.print("# pop(): ");
        System.out.println(sList.pop());
        System.out.print("# pop(): ");
        System.out.println(sList.pop());
        // print stack
        sList.print();
        System.out.println("\n");

        // isEmpty
        System.out.print("# isEmpty(): ");
        System.out.println(sList.isEmpty());
        // print stack
        sList.print();
        System.out.println("\n");

        System.out.print("# pop(): ");
        System.out.println(sList.pop());
        // print stack
        sList.print();
        System.out.println();
    }
}
