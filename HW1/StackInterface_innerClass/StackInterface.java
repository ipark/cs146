/**
 * @author Inhee Park
 * Fall2018.CS146.HW1.Number1.
 *
 * Implmentation of Generic Singly LinkedList 
 * in which contains inner class Node
 * when the following StackInterface is given
 */
public interface StackInterface<T>
{
    public void push(T newEntry);
    public T pop();
    public T peek();
    public boolean isEmpty();
    public void print();
}
