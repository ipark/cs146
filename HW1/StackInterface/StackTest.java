public class StackTest 
{
    public static void main(String[] args) 
    {
        Stack<Integer> sList = new Stack<>();

        System.out.println("push(20)->push(10)->push(40)->push(30)->push(60)");
        sList.push(20);
        sList.push(10);
        sList.push(40);
        sList.push(30);
        sList.push(60);
        sList.print();

        System.out.println("pop() x 2");
        sList.pop();
        sList.pop();
        sList.print();

        System.out.println("peek()");
        System.out.println(sList.peek());
        sList.print();

        System.out.println("pop() x 3");
        sList.pop();
        sList.pop();
        sList.pop();
        sList.print();

        System.out.println("isEmpty()");
        System.out.println(sList.isEmpty());
        sList.print();
    }
}
