public class Node<T>
{
    public T iData;
    public Node<T> next;
    // constructor
    public Node(T id) 
    {
        iData = id;
    }
    // method
    public void print()
    {
        System.out.print("[" + iData + "] ");
    }
}
