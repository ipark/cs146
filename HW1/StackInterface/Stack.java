public class Stack<T> implements StackInterface<T>
{
    private LinkedList<T> stack;
    // constructor
    //
    public Stack()
    {
        stack = new LinkedList<>();
    }
    // pop()
    public T pop() 
    {
        return stack.deleteHead();
    }
    // push()
    public void push(T newEntry) 
    {
        stack.insertHead(newEntry);
    }
    // peek()
    public T peek() 
    {
        return stack.peek();
    }
    // isEmpty()
    public boolean isEmpty() 
    {
        return stack.isEmpty();
    }
    public void print() 
    {
        stack.print();
        System.out.println("---");
    }
}
