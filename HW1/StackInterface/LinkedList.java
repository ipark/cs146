public class LinkedList<T>
{
    private Node<T> head;

    // constructor
    //////////////////
    public LinkedList() 
    {
        head = null;
    }
    //////////////////
    // methods
    public void insertHead(T id)
    {
        // create
        Node<T> newNode = new Node<>(id);
        newNode.next = head;
        head = newNode;
    }
    public T deleteHead() 
    {
        // store current node to temp
        Node<T> temp = head;
        head = head.next;
        return temp.iData;
    }
    public boolean isEmpty() 
    {
        return (head == null);
    }
    public T peek() 
    {
        return head.iData;
    }
    public void print() 
    {
        Node<T> current = head;
        while (current != null) 
        {
            current.print();
            current = current.next;  
        }
        System.out.println();
        System.out.println("LinkedList: head(new)<-tail(old)");
    }
}
