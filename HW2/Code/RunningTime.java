import java.lang.Double.*;
/**
 * @author Inhee Park
 * CS146.HW2.Problem2.
 * Consider two (nested) for-loops 
 * LoopA running time O(n) 
 * LoopB running time O(n^2)
 * Design and implement an experiment to find a value of 'n' 
 * when a LoopB O(n^2) is faster than LoopA O(n)
 */
public class RunningTime {
  /**
   * BigOh_N
   * O(N)time example of summing over single for-loop
   * @param n number of input size
   * @return sum
   */
  public static int BigOh_N(int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) 
      for (int j = 0; j < 10000; j++)
        sum += j;
    return sum;
  }
  /**
   * BigOh_N2
   * O(N^2)time example of summing over double for-loop
   * @param n number of input size
   * @return sum
   */
  public static int BigOh_N2(int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++)
        sum += j;
    }
    return sum;
  }
  /**
   * BigOh_N3
   * O(N^3)time example of summing over triple for-loop
   * @param n number of input size
   * @return sum
   */
  public static int BigOh_N3(int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) 
      for (int j = 0; j < n; j++)
        for (int k = 0; k < n; k++)
          sum += k;
    return sum;
  }
  
  /**
   * comparison_N_N2
   * Comparison of Running Time betwee O(N) vs O(N^2)
   * @param maxN maximum of input size N
   */
  public static void comparison_N_N2(int maxN) {
    double t1_start, t1_stop, t1;
    double t2_start, t2_stop, t2;
    int pow;
    System.out.println("Comparison of Running Time betwee O(N) vs O(N^2)");
    System.out.println("              N         O(N)ms       O(N^2)ms");
    System.out.println("    -----------         ----------   -----------");
    for (int n = 2; n <= maxN; n *= 2) {
      t1_start = System.nanoTime();
      BigOh_N(n);
      t1_stop = System.nanoTime();
      t1 = (t1_stop - t1_start)/1E+6;
  
      t2_start = System.nanoTime();
      BigOh_N2(n);
      t2_stop = System.nanoTime();
      t2 = (t2_stop - t2_start)/1E+6;
  
      pow = (int) (Math.log(n)/Math.log(2));
      if (t1 > t2) 
        System.out.printf("2^(%02d)=\t%7d    %12.5f > %12.5f<--faster!\n", 
                  pow, n, t1, t2);
      else  
        System.out.printf("2^(%02d)=\t%7d    %12.5f < %12.5f\n",
                  pow, n, t1, t2);
    }
  }
  /**
   * comparison_N2_N3
   * Comparison of Running Time betwee O(N^2) vs O(N^3)
   * @param maxN maximum of input size N
   */
  public static void comparison_N2_N3(int maxN) {
   /**
    * Comparison of Running Time betwee O(N^2) vs O(N^3)
    */
   double t2_start, t2_stop, t2;
   double t3_start, t3_stop, t3;
   int pow;
   System.out.println("\n\nComparison of Running Time betwee O(N^2) vs O(N^3)");
   System.out.println("              N         O(N^2)ms     O(N^3)ms");
   System.out.println("    -----------         ----------   -----------");
   for (int n = 2; n <= maxN; n *= 2) {
     t2_start = System.nanoTime();
     BigOh_N2(n);
     t2_stop = System.nanoTime();
     t2 = (t2_stop - t2_start)/1E+6;

     t3_start = System.nanoTime();
     BigOh_N3(n);
     t3_stop = System.nanoTime();
     t3 = (t3_stop - t3_start)/1E+6;
     pow = (int) (Math.log(n)/Math.log(2));
     if (t2 > t3) 
         System.out.printf("2^(%02d)=\t%7d    %12.5f > %12.5f<--faster!\n", 
                 pow, n, t2, t3);
     else  
         System.out.printf("2^(%02d)=\t%7d    %12.5f < %12.5f\n", 
                 pow, n, t2, t3);
    }
	}

  /**
   * Testing for running time comparison
   */
	public static void main(String[] args) {
    int maxN = (int) Math.pow(2, 20);
    comparison_N_N2(maxN);
    comparison_N2_N3(maxN);
  }
}
//12345678901234567890123456789012345678901234567890123456789012345678901234567890
