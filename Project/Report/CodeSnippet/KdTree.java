/**
 * Fall 2018 - CS146 - Project
 * @author INHEE PARK
 */
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.List;
import java.util.ArrayList;

/**
 * Kd-tree where K = 2 dealing with point type data (x, y).
 *
 * 0 Set the initial Rectangle Information (Xmin, Xmax, Ymin, Ymax), 
 *   which will be updated upon insertion of new node
 *
 * 1 Follows BST algorithm (i.e. smaller child goes Left, otherwise Right). 
 *
 * 2 A given node is situated at Even level, compare x's, otherwise y's.
 *
 * 3 At Even Level,  left|right
 *   new node's X < parent's X =>  
 *   decrease Xmax of RectInfo, increase level by 1, and insert it to left
 *   new node's X > parent's X =>  
 *   increase Xmin of RectInfo, increase level by 1, and insert it to right
 *
 * 4 At Odd Level,  right(up)
 *                  --------
 *                  left(down)
 *   new node's Y < parent's Y => 
 *   decrease Ymax of RectInfo, increase level by 1, and insert it to left
 *   new node's Y > parent's Y => 
 *   increase Ymin of RectInfo, increase level by 1, and insert it to right
 *
 * 5 Draw Vertical Line at Odd level;
 *   Draw Horizontal Line at Even level.
 *
 */
public class KdTree extends JPanel { // inherits JPanel to draw using 
                                     // paintComponent() method

  // set the initial range of Rectangular Information
  public static final int INIT_XMIN = 0;     
  public static final int INIT_XMAX = 100;
  public static final int INIT_YMIN = 0;
  public static final int INIT_YMAX = 100;

  public static final int scale = 5; // 5 times scale-up for GUI plot

  // fields
  private Node root;
  private ArrayList<Node> tList = new ArrayList<>(); // aggregated Node list

  // inner class - Node
  private class Node {
    // fields
    private point p;  // (x, y) pair in a separate class
    private RectInfo r; // (Xmin, Xmax, Ymin, Ymax) set info in a separate class
    private Node left; // reference to left child
    private Node right; // reference to right child
    private int level;
    // constructor
    public Node(point p, RectInfo r, int level) {
      this.p = p; // point (x, y)
      this.r = r; // Rectangle Inforamtion (Xmin, Xmax, Ymin, Ymax)
      this.level = level; // 
    }
  } // EO-class-Node

  // constructor for KdTree
  public KdTree() {
    root = null;
  }

  // helper method for insert
  public void insert(point p) {
    // the very first node to be initialized
    int init_level = 0; // thus at root level, start with X-comparison
    root = insert(root, p, 
        new RectInfo(INIT_XMIN, INIT_XMAX, INIT_YMIN, INIT_YMAX), init_level);
    tList.add(root);
    System.out.println("Insert a point " + p.toString() + " to Kd-Tree");
  }
  // private method for insert 
  private Node insert(Node t, point p, RectInfo r, int level) { 
    if (t == null) {
        return new Node(p, r, level); // when t == root == null, create a node
    }
    /////////// Even-Level: compare X's
    if (level % 2 == 0) {
      int comp = p.getX() - t.p.getX();

      if (comp < 0) { // to left; decrease Xmax in r
        int Xmax = t.p.getX(); 
        t.left = insert(t.left, p, 
          new RectInfo(t.r.getXmin(), Xmax, t.r.getYmin(), t.r.getYmax()), 
                      level + 1); // increase level 
        tList.add(t.left);
      }
      else if (comp > 0) { // to right; increase Xmin in r
        int Xmin = t.p.getX();
        t.right = insert(t.right, p, 
          new RectInfo(Xmin, t.r.getXmax(), t.r.getYmin(), t.r.getYmax()), 
                    level + 1); // increase level 
        tList.add(t.right);
      }
      else // do-nothing (comp == 0)
        ; 
    } // EO-Even-level
    /////////// ODD-Level: compare Y's
    else {  
      int comp = p.getY() - t.p.getY();

      if (comp < 0) { // to left; decrease Ymax in r
        int Ymax = t.p.getY();
        t.left = insert(t.left, p, 
          new RectInfo(t.r.getXmin(), t.r.getXmax(), t.r.getYmin(), Ymax), 
                level + 1); // increase level
        tList.add(t.left);
      }
      else if (comp > 0) { // to right; increase Ymin in r
        int Ymin = t.p.getY();
        t.right = insert(t.right, p, 
          new RectInfo(t.r.getXmin(), t.r.getXmax(), Ymin, t.r.getYmax()), 
                level + 1); // increase level
        tList.add(t.right);
      }
      else // do-nothing (comp == 0)
        ;
    } // EO-Odd-Level
    return t;
  } // EO-insert

  // public helper method to determine the kd-tree contains the given point p
  public boolean contains(point p) {
    int init_level = 0;
    boolean b = contains(root, p, init_level);
    System.out.println("The point " + p.toString() + " is contained? " + b);
    return b;
    //return contains(root, p, init_level);
  }

  private boolean contains(Node t, point p, int level) {

    if (t == null) 
      return false;

    if (t.p.getX() == p.getX() && t.p.getY() == p.getY()) 
      return true;

    if (level % 2 == 0) { // even-level; compare X's
      int comp = p.getX() - t.p.getX();
      if (comp < 0) 
        return contains(t.left, p, level + 1);
      else 
        return contains(t.right, p, level + 1);
    } 
    else { // odd-level; compare Y'x
      int comp = p.getY() - t.p.getY();
      if (comp < 0) 
        return contains(t.left, p, level + 1);
      else
        return contains(t.right, p, level + 1);
    }   
  }

  // public helper method to print while traversal of the tree
  public void print() {
    if (root != null)
      print(root);
  }

  private void print(Node t) {
    if (t != null) {
      print(t.left);
      System.out.println(t.p + " : " + t.r);
      print(t.right);
    }
  }

  // for GUI plot, y coordinate should be upside-down
  private int upsideDown(int y) {
    return (INIT_YMAX - y);
  }

  /////////// GUI plot using Swing
  public void paintComponent(Graphics g) {

    Graphics2D g2 = (Graphics2D) g;
    int pointSize = 10;

    g2.setColor(Color.WHITE);
    g2.setStroke(new BasicStroke(4));
    g2.fillRect(INIT_XMIN * scale, INIT_XMAX * scale,
                INIT_YMIN * scale, INIT_YMAX * scale
    );

    for (Node t : tList) { // traversal all the node of Kd-tree
      if (t.level % 2 == 0) { // evenLevel => vertical line
        g2.setColor(Color.CYAN);
        g2.draw(new Line2D.Double( // Vertical Line drawing
          t.p.getX() * scale, upsideDown(t.r.getYmin()) * scale, 
          t.p.getX() * scale, upsideDown(t.r.getYmax()) * scale
        ));
      }
      else { // oddLevel => horizontal line
        g2.setColor(Color.MAGENTA);
        g2.draw(new Line2D.Double( // Horizontal Line drawing
          t.r.getXmin() * scale, upsideDown(t.p.getY()) * scale, 
          t.r.getXmax() * scale, upsideDown(t.p.getY()) * scale
        ));
      }
      // point drawing
      g2.setColor(Color.BLACK);
      g2.fill(new Ellipse2D.Double(
            t.p.getX() * scale, upsideDown(t.p.getY()) * scale, 
            pointSize, pointSize
      ));
      // point Label 
      int offset = 10;
      g2.drawString(t.p.toString(),  // lebel string
          t.p.getX() * scale, upsideDown(t.p.getY()) * scale - offset 
          // label location
      );

    } // EO-for
  } // EO-paintComponent
  
  
} // EO-KdTree

