// helper method for insert
public void insert(point p) {
  //
  // omission
  // 
  /////////// Even-Level: compare X's
  if (level % 2 == 0) {
    int comp = p.getX() - t.p.getX();

    if (comp < 0) { // to left; decrease Xmax in r
      /*****************************
       ** ADD SPECIAL CONDITIONS ***
       *****************************/
      if (level == 0) {
        t.left = insert(t.left, p,  // [0]-----(40,70)----[100]
        new RectInfo(t.r.getXmin(), t.r.getXmax(), t.r.getYmin(), t.r.getYmax()),
                    level + 1); // increase level 
      }
      /*****************************/
      else {
        int Xmax = t.p.getX(); 
        t.left = insert(t.left, p, 
        new RectInfo(t.r.getXmin(), Xmax, t.r.getYmin(), t.r.getYmax()), 
                    level + 1); // increase level 
      }
      tList.add(t.left);
    }
    else if (comp > 0) { // to right; increase Xmin in r
      /*****************************
       ** ADD SPECIAL CONDITIONS ***
       *****************************/
      if (level == 0) {
        int Xmin = t.p.getX();
        int Ymax = t.p.getY();
        t.right = insert(t.right, p, 
        new RectInfo(Xmin, t.r.getXmax(), t.r.getYmin(), Ymax),
                  level + 1); // increase level 
      }
      /*****************************/
      else {
        int Xmin = t.p.getX();
        t.right = insert(t.right, p, 
        new RectInfo(Xmin, t.r.getXmax(), t.r.getYmin(), t.p.getY()), 
                  level + 1); // increase level 
      }
      tList.add(t.right);
    }
    else // do-nothing (comp == 0)
      ; 
  } // EO-Even-level
  /////////// ODD-Level: compare Y's
  //
  // omission
  // 
}
