\documentclass[12pt]{scrartcl}
\renewcommand{\baselinestretch}{1.15} 
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{setspace}
\usepackage[letterpaper,margin=0.7in]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\rhead{ \fancyplain{}{Inhee Park} }
\lhead{ \fancyplain{}{\today} }
\chead{ \fancyplain{}{\thepage}/12}

\cfoot{ \fancyplain{}{}}
\usepackage{lmodern}



%%%%%%%%%%%%%% JAVA CODING BLOCK HIGHLIGHT
\usepackage{listings}
\usepackage{color}
\definecolor{javared}{rgb}{0.6,0,0} % for strings
\definecolor{javagreen}{rgb}{0.25,0.5,0.35} % comments
\definecolor{javapurple}{rgb}{0.5,0,0.35} % keywords
\definecolor{javadocblue}{rgb}{0.25,0.35,0.75} % javadoc
\lstset{language=Java,
basicstyle=\footnotesize\linespread{0.8}\ttfamily,
keywordstyle=\color{javapurple}\bfseries,
stringstyle=\color{javared},
commentstyle=\color{javagreen},
morecomment=[s][\color{javadocblue}]{/**}{*/},
morecomment=[s][\color{javadocblue}]{/*}{*/},
frame=single,
numbers=left,
numberstyle=\ttfamily\tiny\color{black},
stepnumber=1,
showspaces=false,
showstringspaces=false,
morekeywords={List, String, Integer, Double, LinkedList},
}
%%%%%%%%%%%%%% JAVA CODING BLOCK HIGHLIGHT
%opening
\title{Implementation of 2-dimensional Kd-tree and GUI plot using Java Swing} 
\author{CS146 - Fall 2018 - Final Project}
\date{Inhee Park}

\begin{document}
\maketitle
\thispagestyle{empty}
%///////////////////////////////////////////////////////////////
\hrule
\begin{center}
\includegraphics[width=\textwidth]{./Figs/projectQ.png}\\
\includegraphics[width=6cm]{./Figs/projectF.png}
\end{center}
\hrule

\section{INTRODUCTION}
A k-dimensional tree (aka kd-tree) is one kind of binary search tree (BST)
in that if the current node's element is less than the parent node's element, 
it would be a left child, otherwise, a left child. A distinct feature of kd-tree 
from BST is that each node can represent k-dimensional data point, where $k \ge 1$.
Conversely, if $k = 1$, then a kd-tree is a BST. 
Non-leaf nodes of kd-tree correspond to a hyper-plane which is divided into 
left sub-tree and right sub-tree by a comparison of points between parent-child 
according to the level information of a parent's node.

As the kd-tree can represent the high-dimensional data, 
it is frequently used in the fields of astronomy and machine learning in which demand
high-dimensional data analyses techniques (i.e. dimensionality reduction) for correct 
classification of the new data point based on the collected set of data.
Owing to the close relation between the k-dimensional space and a kd-tree,
possible applications of kd-tree are range search (i.e. finding all points within a given range)
and a nearest-neighbor search (i.e. finding the closest point to a given point).

In this final project of CS146, we choose k to be 2, i.e. 2-dimensional
kd-tree (in short, ``2d-tree'') whose node contains \texttt{(x, y)} pair coordinate.
The first insertion point to the empty tree is the tree's root, whose level is 0,
i.e. even level.  When the level of the current node is even, 
subtract its x-coordinate from that of the next point to be inserted.
If the subtraction is less than zero, insert the next point to the left child of the current node.
Otherwise, insert into the right child.  Then increase the level by one for the newly inserted
node, so that the next points to be inserted alternatively compares its x-coordinate or 
y-coordinate according to the evenness or oddness of the level, respectively.  

Beside plotting a data point that was inserted according to the kd-tree algorithm, we need to demonstrate how the kd-tree bisects the 2-d space into left/right along with a vertical line (in case of odd level of the node); similarly, up/down along with a horizontal line (in case of even level of the node).  To do so, we need to keep updating the rectangle information of the space, i.e. minimum and maximum of x- and y-coordinates at each time of insertion. For example, given that the level of the node is even, if this would be a left child, then whose minimum value of x-coordinate needs to be increased with its parent's x-coordinate, otherwise, maximum value of x-coordinate needs to be decreased with it. To achieve this plotting tasks, Java's Swing package is used by inheriting \texttt{JPanel} class for the \texttt{kd-tree} class, so that \texttt{PaintComponent()} method of \texttt{JPanel} is conveniently used \cite{L1}.

Regarding implementation of the kd-tree algorithm and corresponding vertical/horizontal line drawing in GUI environment, there are two prerequisite data structures to be implemented: (1) a data structure to carry \texttt{(x, y)} coordinate as a single pair, which will be a class \texttt{point}; (2) a data structure to store and update the rectangle information upon new insertion, which will be a class \texttt{RectInfo}. 

To test correct implementation of the kd-tree, (1) provide a new data point and test whether it is contained in the kd-tree or not; (2) draw data points along with corresponding vertical/horizontal lines by accessing the updated space information stored in the \texttt{RectInfo}.

%body - source code (comment) & sample runs (gui output sample)
%conclusion - what you've learned & summary
\newpage
\section{SOURCE CODE}
\subsection{Program}
The program of the kd-tree enabling a GUI drawing consists of the 4 classes as following including a tester in which has \texttt{main()} method: 
\begin{verbatim}
KdTree/
      +-- KdTreeTester.java // main()
      |
      +-- KdTree.java       // kd-tree implmentation
      |      
      +-- point.java        // data structure for (x,y) pair 
      |
      +-- RectInfo.java     // data structure for rectagle information
\end{verbatim}


\subsection{\texttt{KdTreeTester.java}}
Five pairs of \texttt{(x, y)} coordinates from the given example of the project statement were used to test the kd-tree implementation:
\begin{verbatim}
    (50, 40)             <---- level 0 
       /\
      /  \
 (40,70) (80,20)         <---- level 1
           /\
          /  \
     (90,10) (60,30)     <---- level 2
     
\end{verbatim}
\lstinputlisting{./CodeSnippet/KdTreeTester.java}

\subsection{\texttt{KdTree.java}}
This is a core class of Kd-Tree algorithm.
As the Kd-tree is a BST, most of code is re-used from the  \texttt{BST.java} class from the Homework4 of CS146 in that
consisting of inner class \texttt{Node} to build up \texttt{KdTree} structure. However, in kd-tree members of the \texttt{Node} class 
are higher dimensional than the \texttt{Node} of \texttt{BST}.
Corresponding code snippets are in line number from 55---67:
\begin{description}
\item[\texttt{point p}] i.e. \texttt{(x,y)} coordinate pair structure, which is a separate class.
\item[\texttt{RectInfo r}] whose members \texttt{Xmin, Xmax, Ymin, Ymax}, which is a separate class.
\item [\texttt{int level}] whose mode of 2 is used to determine x-comparison or y-comparison, as well as vertical line drawing or horizontal line drawing.  
\end{description}

The \texttt{KdTree} class has several methods, similar to the \texttt{BST} class, to traversal all nodes from root to leaf nodes, most methods are paired, i.e. a \texttt{public} method without a parameter accessing the internal structure of the \texttt{Node} is paired up with
a \texttt{private} class which can access internal data structure of the \texttt{Node} such as \texttt{root}:
\begin{description}
\item[\texttt{public void insert(point p)}] --- public helper method to take a data point from user, and call the private \texttt{insert} method starting with a \texttt{root}.
\item[\texttt{public Node insert(Node t, point p, RectInfo r, int level)}] --- private method to insert a data point and update a space information using \texttt{RectInfo} data structure.
\item[\texttt{public boolean contains(point p)}] --- public helper method to take a data point from user, and call the private \texttt{contains} method to return a boolean whether the kd-tree contains a given data or not.
\item[\texttt{private boolean contains(Node t, point p, int level)}] --- private method to return a boolean whether the given point is contained in the kd-tree. In doing so, likewise in the \texttt{insert} method, \texttt{level} information is used to determine x-comparison or y-comparison.
\item[\texttt{public void print()}] --- public helper method to traversal kd-tree by printing the node, just debugging purpose 
\item[\texttt{private void print(Node t)}] --- private method  to traversal kd-tree by printing the node, just debugging purpose 
\item[\texttt{private int upsideDown(int y)}] --- Swing (GUI package) uses a point of left, top corder as an origin, which is counter-intuitive, thus make an utility method to convert y-coordinate upside down by subtracting the given y-coordinate from the maximum value of y-coordinate. 
\item[\texttt{public void paintComponent(Graphics g)}] --- in general, this method is not directly called, instead is called as  \texttt{repaint()} under the \texttt{JPanel} class.  However, \texttt{repaint()} resets the background at each call, thus calling it at every data point erases the previous drawing. One possible workaround is to define a collective data structure:
\texttt{private ArrayList<Node> tList = new ArrayList<>()} 
and keep adding the \texttt{Node} object at each \texttt{insert} call, and provide the \texttt{ArrayList<Node>} data to the \texttt{paintComponent(Graphics g)} method. Finally traversal of the aggregated data structure, draw lines and points by accessing the object's data.
\end{description}
\lstinputlisting{./CodeSnippet/KdTree.java}

\subsection{\texttt{point.java}}
\lstinputlisting{./CodeSnippet/point.java}

\subsection{\texttt{RectInfo.java}}
\lstinputlisting{./CodeSnippet/RectInfo.java}

\section{RESULT}
\subsection{General 2d-tree output}
As shown in Fig \ref{f:swing}, both containment test by calling \texttt{contains(point p)} and GUI representation of the kd-tree structure are successful. At each data point (in filled black circle) either vertical line (in cyan color) or horizontal line (in magenta) splits the space either left/right or up/down, respectively.  Those lines can't across with each other because when inserting a point, a rectangle information (i.e. space information) is updated either decreasing a maximum coordinate or increasing a minimum coordinate.
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{./Figs/kdTree-result.png}
	\caption{Screenshot of execution output of \texttt{KdTreeTester} with
  generalized kd-tree algorithm.
	Left side shows results of \texttt{contains(point p)} method calls; 
	Right side shows result of GUI plot using Swing.}
\label{f:swing}
\end{figure}


\subsection{Modified 2d-tree output}
Note that, however, I couldn't exactly reproduce the kd-tree figure denoted by red arrows in the project statement in Fig \ref{f:swing2}(a), where a horizontal line with point \texttt{(40, 70)} across the Vertical line with point \texttt{(50, 40)}. Hence, in order to exactly reproduce the sample figure, I have to modify the generalized kd-tree algorithm by adding special conditions in case of level 0 to redefine rectangle information.  Resulting reproducible figure is shown in the Fig \ref{f:swing2}(b).
\begin{figure}[h]
  \includegraphics[width=0.25\textwidth]{./Figs/pplot.png}
  \includegraphics[width=0.75\textwidth]{./Figs/kdTree-result2.png}
  \caption{Sample plot of 2d-tree provided in the Project statement (a);
    Screenshot of execution output of \texttt{KdTreeTester} with modified kd-tree algorithm (b)}
\label{f:swing2}
\end{figure}

Corresponding code snippet shows the modification of even level pertinent part of the \texttt{insert} method of the \texttt{KdTree.java}. 
\lstinputlisting{./CodeSnippet/KdTree2.java}

\section{CONCLUSION}
This 2d-tree project requires extended knowledge of Binary Search Tree: defining inner class Node with multi-dimensional data structure instead of single data point, traversal of Node in a recursive manner using a pair of public helper method and corresponding private method for insertion as well as containment test). It also requires a Graphic User Interface knowledge to display the constructed 2d-tree to aid understanding of relationship between 2d-tree and 2-dimensional space splitting, suggesting further applications to the range search (i.e. finding all points within a given range) and 
a nearest-neighbor search (i.e. finding the closest point to a given point).
Lastly, the performance of the 2d-tree with N nodes is $O(\lg N)$ in average case; and $O(N)$ in worst case.

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BIBLOIGRAPHY %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{00}
	\bibitem{L1} C. Horstmann, Object-Oriented Design \& Patterns, 3rd edition. (Textbook for CS151)
\end{thebibliography}
\end{document}
