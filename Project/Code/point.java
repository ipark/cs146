/**
 * Fall 2018 - CS146 - Project
 * @author INHEE PARK
 *
 * Data structure to deal with (x, y) pair coordinate
 *
 */
public class point {
  // fields
  private final int x;
  private final int y;

  // constructor
  public point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  // getters
  public int getX() {
    return x;
  }
  public int getY() {
    return y;
  }

  @Override
  public String toString() {
    return "(" + x + ", " + y + ")";
  }
}
