/**
 * Fall 2018 - CS146 - Project
 * @author INHEE PARK
 *
 * Data structure to deal with Rectangle coordinates
 * (Xmin, Xmax, Ymin, Ymax)
 */
public class RectInfo {
  
  // fields
  private int Xmin;
  private int Xmax;
  private int Ymin;
  private int Ymax;

  // constructor
  public RectInfo(int Xmin, int Xmax, int Ymin, int Ymax) {
    this.Xmin = Xmin;
    this.Xmax = Xmax;
    this.Ymin = Ymin;
    this.Ymax = Ymax;
  }

  // set of getters
  public int getXmin() { return Xmin; }
  public int getXmax() { return Xmax; }
  public int getYmin() { return Ymin; }
  public int getYmax() { return Ymax; }

  @Override
  public String toString() {
    return "X=[" + Xmin + ", " + Xmax 
       + "];Y=[" + Ymin + ", " + Ymax + "]";
  }

}
