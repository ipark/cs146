/**
 * Fall 2018 - CS146 - Project
 * @author INHEE PARK
 *
 * Kd-tree tester
 * 1) containment test by providing a new data point
 * 2) kd-tree drawing
 */
import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

public class KdTreeTester {

  // GUI frame size
  public final static int FRAME_WIDTH = 500;
  public final static int FRAME_HEIGHT = 500;

  public static void main(String[] args) {

    // instantiation of the KdTree class
    KdTree kdtree = new KdTree();

    // insert 5 pairs of (x, y) coordiate
    kdtree.insert(new point(50, 40)); 
    kdtree.insert(new point(40, 70));
    kdtree.insert(new point(80, 20));
    kdtree.insert(new point(90, 10));
    kdtree.insert(new point(60, 30));

    // containment test
    // kd-tree contains p; should printout TRUE
    point p = new point(40, 75); 
    kdtree.contains(p);

    // containment test
    // kd-tree does NOT contains q; should printout FALSE
    point q = new point(40, 70); 
    kdtree.contains(q);

    JFrame frame = new JFrame();
    frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);

    // KdTree(subclass)--> JPanel(superclass)
    // JPanel with KdTree is added to the JFrame
    frame.add(kdtree); 

    // action defined after exit
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
